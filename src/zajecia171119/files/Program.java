package zajecia171119.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Program {
    public static void main(String[] args) {


        System.out.println("Odczyt z pliku: ");

        String line;
        BufferedReader reader = null;
        FileReader filereader = null;

        try {
            filereader = new FileReader("Notatka.txt");
            reader = new BufferedReader(new FileReader("Notatka.txt"));
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) ;
            {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
             if (filereader != null) ;
            {
                try {
                    filereader.close();
                } catch (IOException e) {
                    e.printStackTrace();
            }
        }

    }
}
