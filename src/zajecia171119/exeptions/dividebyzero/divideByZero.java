package zajecia171119.exeptions.dividebyzero;

import java.util.InputMismatchException;
import java.util.Scanner;

public class divideByZero {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj pierwszą liczbę");
        int liczba = scanner.nextInt();
        System.out.println("Podaj drugą liczbę");
        int drugaLiczba = scanner.nextInt();

        System.out.println("Wynik dzielenia");
        try {
            int result = liczba / drugaLiczba;
            System.out.println("Wynik: " + result);
        } catch (ArithmeticException ex) {
            System.out.println("Nie można dzielić przez 0!");
        } catch (InputMismatchException ex) {
            System.out.println("Wprowadz liczbę");
        } catch (Exception ex) {
            System.out.println("Wystapił błąd: " + ex.getMessage());
        }
}
}
