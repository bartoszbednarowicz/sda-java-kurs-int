import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Witaj w programie");
        System.out.println("Podaj nazwę firmy: ");
        String name = sc.nextLine();
        Firma mojaFirma = new Firma(name);

        boolean exit = false;
        if (!exit) {
            printMenu();
            int wybor = sc.nextInt();
            switch (wybor){
                case 1:
                    add(mojaFirma);
                    print(mojaFirma);
                    printMenu();
                case 2:
                    print(mojaFirma);
                    printMenu();
                case 5:
                    break;
                default:
                    break;
            }
        }

    }

    public static void add(Firma mojaFirma) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj imie");
        String empName = sc.nextLine();
        System.out.println("Podaj nazwisko");
        String empSurname = sc.nextLine();
        System.out.println("Podaj wiek");
        int empAge = sc.nextInt();

        Pracownik pracownik = new Pracownik(empName, empSurname, empAge);
        boolean isSuccess = mojaFirma.addPracownik(pracownik);

        if (isSuccess) {
            System.out.println("Dodano");
        } else {
            System.out.println("nie udalo sie dodac");
        }
    }

    public static void printMenu() {
        System.out.println("Wybierz opcje: ");
        System.out.println("1. Dodawanie pracownika: ");
        System.out.println("2. Wyswietlanie pracownikow: ");
        System.out.println("3. Import pracownikow");
        System.out.println("4. Zapisz pracownikow do pliku");
        System.out.println("Wybor: ");
    }

    public static void print(Firma mojaFirma) {
        for (Pracownik e : mojaFirma.getPracownicy()) {
            if (e != null) {
                System.out.println(e.getDescription());
            }
        }
    }
}
