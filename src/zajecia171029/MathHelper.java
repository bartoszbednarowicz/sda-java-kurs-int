package zajecia171029;

public class MathHelper {

    /**
    * Metoda obliczająca n! CTRL+Q
    * */

    public static int factorial(int n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result *= i;

        }   return result;
    }
    /**
    * Metoda obliczająca pole trójkąta wg wzoru Herona
    * */

    public static double obliczPole(double a, double b, double c) {
        double p = a + b + c / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    public static int fibonacci (int n) {
        int fib1 = 0;
        int fib2 = 1;
        int wynik = 0;

        for (int i = 3; i <= n; i++) {
            wynik= fib1 + fib2;
            fib1 = fib2;
            fib2= wynik;
        }
        return wynik;
    }

}


