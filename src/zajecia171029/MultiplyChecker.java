package zajecia171029;

import java.util.Random;
import java.util.Scanner;

public class MultiplyChecker {
    public static void main(String[] args) {

        boolean isCorrect = false;
        System.out.println("Losuje dwie liczby.. Podaj wynik mnożenia");
        Random random = new Random();
        Scanner sc = new Scanner(System.in);

        int liczba1 = random.nextInt(10);
        int liczba2 = random.nextInt(10);

        System.out.println(liczba1);
        System.out.println(liczba2);

        while (isCorrect == false) {

            System.out.println("Podaj wynik mnożenia " + liczba1 + ' ' + '*' + ' ' + liczba2);

            int wynik = sc.nextInt();

            if (wynik == liczba1 * liczba2) {
                System.out.println("Poprawna odpowiedź");
                isCorrect = true;
            } else {
                System.out.println("Błędna odpowiedź");
            }
        }
        System.out.println("Gratulacje!");
    }
}
