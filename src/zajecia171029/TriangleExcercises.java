package zajecia171029;

import java.util.Scanner;

public class TriangleExcercises {


    public static void main(String[] args) {
//        wczytac 3 dlugosci typu double

        double bokA;
        double bokB;
        double bokC;

        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj bok a");
        bokA = sc.nextDouble();

        System.out.println("Podaj bok b");
        bokB = sc.nextDouble();

        System.out.println("Podaj bok c");
        bokC = sc.nextDouble();


//        czy mozna zbudowac trojkat?

        if ((bokA + bokB > bokC) && (bokB + bokC > bokA) && (bokC + bokA > bokB)) {

            System.out.println("Trójkąt może być zbudowany");
            double p = (bokA + bokB + bokC) / 2;
            double pole = Math.sqrt(p * (p - bokA) * (p - bokB) * (p - bokC));
            System.out.println("Pole trójkąta wynosi: " + String.format("%.2f", pole));
        } else {
            System.out.println("Nie można zbudować trójkąta");
        }

    }
}
