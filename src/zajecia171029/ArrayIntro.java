package zajecia171029;

public class ArrayIntro {
    public static void main(String[] args) {

        int[] tab = new int[10];
//        tab[0] = 25;
//        tab[1] = 20;
//        tab[2] = 50;
//
//        int pierwszy = tab[0];
//        System.out.println(pierwszy);

//        wypelnic tablice elementami, w kazdym indekse ma byc wartosc plus 20

        for (int i = 0; i < tab.length; i++) {
            tab[i] = i + 20;
        }

//        petla drukujaca wartosci
        for (int i = 0; i < tab.length; i++) {
            System.out.println("Indeks: " + i + " Wartość: " + tab[i]);

//        policzyć sumę elementów w tablicy
        }
            int suma = 0;
            for (int i = 0; i < tab.length; i++) {
                int elem = tab[i];
                suma += elem;
//        szybciej: suma += tab[i];
            }
            System.out.println("Suma elementów tablicy: " + suma);

            double srednia = 0;
            for (int i = 0; i < tab.length; i++) {
                srednia = (double)suma / tab.length;
            }
            System.out.println("Srednia: " + srednia);
    }
}
