package zajecia171029;

import java.util.Scanner;

public class TriangleMethod {



    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        double b = sc.nextDouble();
        double c = sc.nextDouble();

        if ((a + b > c) && (b + c > a) && (c + a > b)) {

            System.out.println("Trójkąt może być zbudowany");
            double pole = MathHelper.obliczPole(a, b, c);
            System.out.println("Pole trójkąta wynosi: " + String.format("%.2f", pole));
        } else {
            System.out.println("Nie można zbudować trójkąta");
        }
    }
}
