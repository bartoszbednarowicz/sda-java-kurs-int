package zajecia171029;

import java.util.Random;

public class FindMaxInArray {
    public static void main(String[] args) {

        int[] array = new int[10];
        Random rd = new Random();

//        wpisywanie losowych wartosci

        for (int i = 0; i < array.length; i++) {
            array[i] = rd.nextInt(50);
        }

//        wypisz wartosci
        for (int i =0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
//        szukanie max, zakladamy wstepnie ze najwieksza wartosc jest w tablicy o indeksie 0
        int max = array[0];
        for (int i = 1; i <array.length; i++) {
            if(array[i] > max) {
                max=array[i];
            }

        }
        System.out.println("\nNajwiesza wartosc to: " + max);
    }
}
