public class Pracownik {

    private String name;
    private String surname;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {this.age = age;}


    public Pracownik(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Pracownik(String name, String surname, int age) {
        this(name, surname);
        this.age = age;
    }

    public String getDescription() {
        return String.format("Name: %s, Surname: %s, Age: %s, ", name, surname, age);
    }



}
