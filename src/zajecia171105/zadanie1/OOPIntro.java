package zajecia171105.zadanie1;

import zajecia171105.zadanie1.Person;
import zajecia171105.zadanie1.Rectangle;

public class OOPIntro {
    public static void main(String[] args) {
        Person ja = new Person("Bartek", "Bednarowicz");
        ja.name = "Bartek";
        ja.surname = "Bednarowicz";
        ja.age = 33;
        ja.email = "poczta";

        Person ty = new Person("Adam", "Kowalski");
        ty.name = "Adam";
        ty.surname = "Kowalski";
        ty.age = 25;
        ty.email = "poczta";

        ja.sayHello();
        ty.sayHello();

        Rectangle r1 = new Rectangle();
        r1.setA(4);
        r1.setB(5);
        Rectangle r2 = new Rectangle();
        r2.setA(10.3);
        r2.setB(12);
        Rectangle r3 = new Rectangle();
        r3.setA(4);
        r3.setB(5);

        System.out.println(r1.field());
        System.out.println(r2.field());
        System.out.println(r3.field());

        System.out.println(r1.obwod());
        System.out.println(r2.obwod());
        System.out.println(r3.obwod());




    }
}
