package zajecia171105.zadanie1;

//  klasa Person do reprezentacji danych o osobie- obiekcie

public class Person {

//  stan mówi o tym, jaka ta osoba/obiekt jest

    public String name;
    public String surname;
    public int age;
    public String email;

//    konstruktory - 1
    public Person (String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
//konstruktory - 2
    public Person (String name, String surname, int age) {
        this (name, surname);
        this.age = age;
    }

//    konstruktory 3
    public Person (String name, String surname, int age, String email){
        this(name, surname, age);
        this.email = email;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //   zachowanie - czynnosc na osobie - do tego sluzy metoda

    public void sayHello() {
        System.out.println("Jestem " + name + " " + surname);
    }

}
