package zajecia171105.zadanie1;

public class Rectangle {
    private double a;
    private double b;


//    metoda obliczajaca pole

    public double field() {
        return a*b;
    }

//    metoda obliczajaca obwod
    public double obwod() {
        if (a <= 0) {
            throw new IllegalArgumentException("Dlugosc mniejsza od zera");
        }
        return 2*(a+b);
    }
//    getter i setter dla pola a i b
    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;  // this czyli referencja do obiektu ktory wywolal te metode
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }
//alt instert

}
