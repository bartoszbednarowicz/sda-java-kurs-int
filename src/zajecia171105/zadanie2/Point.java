package zajecia171105.zadanie2;

public class Point {
    private double x;
    private double y;

//    konstruktor - sluzy do inicjalizowania obiektu

    public Point() {
        System.out.println("Tworzę nowy obiekt...");
    }

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double distanceFromOrigin() {
        return Math.sqrt(2*x + 2*y);
    }


}
