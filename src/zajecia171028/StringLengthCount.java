package zajecia171028;

//Zadanie 4 - program zczytujący ilość znków w linii tekstu

import java.util.Scanner;

public class StringLengthCount {
    public static void main(String[] args) {

//        Utworzyc skaner

        Scanner sc = new Scanner(System.in);

//        Zapytac o napis

        System.out.println("Napisz ciąg znaków");
        String line = sc.nextLine();

//        wyswietlic menu 1. z bialymi znakami 2 bez bialych znakow

        System.out.println("Wybierz opcję");
        System.out.println("1 - policz dlugosc z białymi znakami");
        System.out.println("2 - policz dlugosc bez białych znaków");

        int wybor = sc.nextInt();


//        switch case
        switch (wybor) {
            case 1:
                System.out.println(line.length());
                break;

            case 2:
                int licznik = 0;
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) != ' ' && line.charAt(i) != '\t') {
                        licznik++;
                    }
                }
                System.out.println("Liczba znaków: " + licznik);
                break;

            default:
                System.out.println("Zły wybór"); }

        }
    }


