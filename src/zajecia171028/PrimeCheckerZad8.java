package zajecia171028;

import java.util.Scanner;

public class PrimeCheckerZad8 {

    public static boolean isPrime (int number) {
        boolean result = true;
        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                result = false;
                break;
            }

        } return result;
    }

    public static void main(String[] args) {

        System.out.println("Podaj liczbę");
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();
        boolean wynik = isPrime(liczba);
        System.out.println("Czy jest pierwsza? " + liczba + " wynik: " + isPrime(liczba));
    }
}
