package zajecia171028;

/*  Wypisać parzyste z zakresu 0-100
*/
public class DoWhile {
    public static void main(String[] args) {

        int licznik = 0;
        do{
            if (licznik % 2 !=0)
            System.out.println(licznik);
            licznik++;
        } while (licznik<100);

    }
}
