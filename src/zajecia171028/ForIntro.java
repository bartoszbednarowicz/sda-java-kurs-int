package zajecia171028;

public class ForIntro {
    public static void main(String[] args) {
        System.out.println("liczby rosnąco");
        for (int i = 0; i < 500 && i % 5 == 0; i += 5) {
            System.out.println(i);

        }
        System.out.println("liczby malejąco");
        for (int j = 500; j > 0 && j % 5 == 0; j -= 5) {
            System.out.println(j);


//            można też zapisać to w taki sposób: for ( ; i > 0 && i & 5 == 0; 1 -=5)
        }
    }
}