package zajecia171028;

public class CharDisplayIntroZad6 {
    public static void main(String[] args) {

//        Tablica ASCII - tablica znaków przyporządkowujaca znakom ich wartosci liczbowe

        for (char c = 'A'; c <= 'Z'; c++) {
            System.out.println("Pozycja: " + (short) c + " znak: " + c);
        }

        for (char c = 'Z'; c >= 'A'; c--) {
            System.out.println("Pozycja: " + (short) c + " znak: " + c);
        }


        char c = 'A';
        char d = 'Z';

        while (c < d) {
            System.out.println((short) c + " znak: " + c);
            c++;
        }
        System.out.println("Koniec");
    }
}
