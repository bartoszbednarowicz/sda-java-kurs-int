public class Firma {

    public static final int DEFAULT_SIZE = 20;
    private String imienazwisko;
    private Pracownik[] pracownicy;
    private int companySize = 0;

    public String getImienazwisko() {
        return imienazwisko;
    }

    public void setImienazwisko(String imienazwisko) {
        this.imienazwisko = imienazwisko;
    }

    public Pracownik[] getPracownicy() {
        return pracownicy;
    }

    public Firma(String imienazwisko) {
        this.imienazwisko = imienazwisko;
        this.pracownicy = new Pracownik[DEFAULT_SIZE];
    }

    public Firma(String imienazwisko, int initialSize) {
        this.imienazwisko = imienazwisko;
        this.pracownicy = new Pracownik[initialSize];
    }

    public boolean addPracownik(Pracownik emp) {
        if (companySize < pracownicy.length) {
            pracownicy[companySize++] = emp;
            return true;
        }
        return false;
    }

    public void addPracownicy(Pracownik[] pracownicy) {
        for (int i = 0; i < pracownicy.length; i++) {
            this.addPracownik(pracownicy[i]);
        }


    }

}
