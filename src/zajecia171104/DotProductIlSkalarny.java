package zajecia171104;

import java.util.Random;


public class DotProductIlSkalarny {

//    deklaracja stalej - zeby rozmiar tablicy byl w tym przypadku rowny i niezmenialny.
//
    public static final int ARRAY_SIZE = 30;
    public static final int RANDOM_UPPER_BOUND = 50;

    public static void main(String[] args) {

        int[] arr = new int[ARRAY_SIZE];
        int[] arr2 = new int[ARRAY_SIZE];
        Random rd = new Random();

//  losowanie wartosci

        for (int i = 0; i < ARRAY_SIZE; i++) {
            arr[i] = rd.nextInt(RANDOM_UPPER_BOUND) + 1;
            arr2[i] = rd.nextInt(RANDOM_UPPER_BOUND) + 1;
        }
//  Oblicznie wartosci

        int suma = 0;
        for (int i = 0; i < ARRAY_SIZE ; i++) {
            suma += arr[i] * arr2[i];

            System.out.print(arr[i] + " ");
            System.out.print(arr2[i] + " ");

        }
        System.out.println("Iloczyn skalarny wektorów: " + suma);
    }

}



