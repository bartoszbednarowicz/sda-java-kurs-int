package zajecia171104;

import java.util.Random;

public class CountMaxNumber {
    public static void main(String[] args) {


//        jesli chcielibysmy losowy rozmiar tablicy to

//        Random rd = new Random();
//        int size = rd.nextInt;
//        int[] tablica = new int[size];

//        wypelnic losowymi wartosciami

        int[] tablica = new int[500];
        Random rd = new Random();

        for (int i = 0; i < tablica.length; i++) {
            tablica[i] = rd.nextInt(30);
        }

//        znalezc max

        int max = tablica[0];
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] > max) {
                max = tablica[i];
            }
        }
        System.out.println("Największa liczba w tablicy to " + "\n" + max);

//        zliczyc ile razy sie pojawia

        int ile = 0;
        for (int i = 0 ; i < tablica.length; i++) {
            if (tablica[i] == max) {
                ile++;
            }
        }
        System.out.println("Ilość wystąpień w tablicy: " + ile);
    }
}
