package zajecia171104;

import java.util.Scanner;

public class ArrayZadanie9 {

    public static boolean isPalindrome(String text) {
        text = text.replace(" ", "").toLowerCase();
        boolean result = true;
        for (int i = 0; i < text.length() /2 ; i++) {
            if (text.charAt(i) != text.charAt(text.length() - 1 - i)) {
               result = false;
            }
        }
        return result;
    }


    public static void main(String[] args) {

//        wczytaj napis od uzytkownika - zbuduj skaner, zapytaj o napis

        String text = "";
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz napis do 30 znaków");
        text = sc.nextLine();
        System.out.println("Napis " + text + " Wynik:" + isPalindrome(text));

    }
}
