package zajecia171104;

import java.util.Random;
import java.util.Scanner;

public class ArrayZadanie5 {
    public static void main(String[] args) {
//        utworzyc tablice 30 elementow

        int[] t = new int[30];

//        wypelnic losowymi wartosciami

        Random rd = new Random();

        for (int i = 1; i < t.length; i++) {
            t[i] = rd.nextInt(50);
        }

//        uzytkownik wprowadza liczbe

        int x;
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe, sprawdze czy wystepuje w tablicy");
        x = sc.nextInt();


//        sprawdzamy czy ona wystepuje w tablicy


//        Ten fragment przeniesiony do metody
//        boolean czyObecna = false;
//
//        for (int i = 0 ; i < t.length ; i++) {
//            if (t[i] == x) {
//                czyObecna = true;
//                break;
//            }


        boolean wynik = ArrayHelper.isPresent(t, x);
        if (wynik) {
            System.out.println("Występuje w tablicy");
        } else {
            System.out.println("Nie wystepuje w tablicy");
        }

//      Przydatne wyrazenie
//      wyrazenie ? wykona-sie : nie-wykona-sie

        System.out.println(wynik ? "Występuje w tablicy" : "Nie wystepuje w tablicy" );

    }

}
