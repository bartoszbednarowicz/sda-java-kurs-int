package zajecia171104;

import java.util.Scanner;

public class MatrixIntro {

    //  co powinno byc zwracane jesli nie pasuja wymiary
//      dopisac pobieranie rozmiarow dla pierwszej i drugiej

    public static int[][] createMatrix() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rozmianr pierwszej macierzy");
        System.out.println("podaj ilosc wierszy");
        int mx = scanner.nextInt();
        System.out.println("Podaj ilosc kolumn");
        int my = scanner.nextInt();

        return new int[mx][my];
    }


//    wypelnianie

    public static void fillMatrix(int[][] m) {

        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                m[i][j] = scanner.nextInt();
            }
        }
    }
//wyswiietlanie zawartosci

    public static void printMatrix(int[][] matrix) {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println();
    }

    //    dodanie macierzy, sprawdzenie niepasujacych macierzy

    public static int[][] addMatrix(int[][] a, int[][] b) {
        if (validate(a, b) == false) {
            return null;
        }
        int[][] result = new int[a.length][a[0].length];
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
            System.out.println();
        }
        return result;
    }

    private static boolean validate(int[][] a, int[][] b) {
//        jezeli zewnetrzne wymiary sa rozne - falsz, tabele sa rozne
        boolean areSame = true;
        if (a.length != b.length) {
            return false;
        }
//        jesli wymiary zewnerzne sa rowne, sprawdzamy wiersze
//        jesli trafimy na wierz o roznej ilosci kolumn to bedzie falsz i tabele nie sa takie same
            for ( int i = 0 ; i < a.length ; i++) {
                if (a[i].length != b[i].length){
                    areSame = false;
                }

        }
        return true;
    }

    public static void main(String[] args) {


//        utworzenie maciezy 3x3
        int[][] matrix = createMatrix();
        int[][] secondMatrix = createMatrix();

////        wypelnienie wartosciami od uzytkownika
        System.out.println("Podaj liczby");

//        for (int i = 0; i < matrix.length; i++) {
//            for ( int j = 0 ; j < matrix[i].length; j++) {
//                matrix[i][j] = scanner.nextInt();
//
//            }
//        }  LUB>

        fillMatrix(matrix);
        fillMatrix(secondMatrix);


////        wyswietlenie zawartosci
//
//        for (int i = 0; i < matrix.length; i++) {
//            for (int j = 0; j < matrix[i].length; j++) {
//                System.out.print(matrix[i][j] + " | ");
//            }
//            System.out.println(); LUB:

        printMatrix(matrix);
        printMatrix(secondMatrix);

        int[][] result = addMatrix(matrix, secondMatrix);
        if (result != null) {
            printMatrix(result);
        } else {
            System.out.println("Nie można dodac tych macierzy");
        }
    }
}


