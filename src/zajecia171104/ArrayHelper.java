package zajecia171104;

public class ArrayHelper {
    public static boolean isPresent(int[] array, int element) {
        boolean present = false;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                present = true;
                break;
            }
        }
        return present;
    }

    /*
    * Metoda wyswietlajca wszystkie elementy w tablicy*/

    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i] + " ");
        }
        System.out.println();
    }


}
