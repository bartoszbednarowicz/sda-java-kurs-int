package zajecia171104;

import java.util.Scanner;

public class SearchStringInArray {
    public static void main(String[] args) {

//        utworzyc tablice 5 napisow

//        Wersja mniej porpawna, jesli znamy wyrazenia:
//        String[] nazwiska = new String[5];

//        Wersja bardziej poprawna:
        Scanner sc = new Scanner(System.in);
        String[] surnames = {"Kowalski", "Nowak", "Adamiak", "Test", "Nazwisko"};

//        i ja zainicjalizowac
//        zapytac o nazwisko
//        odpowiedzie czy wystepuje


        System.out.println("Podaj nazwisko");
        String nazwisko = sc.nextLine();
        boolean isPresent = false;
        for (int i = 0; i < surnames.length; i++) {
            if (surnames[i].equals(nazwisko)) {
                isPresent = true;
                break;
            }
        }
        System.out.println(isPresent);
    }
}
