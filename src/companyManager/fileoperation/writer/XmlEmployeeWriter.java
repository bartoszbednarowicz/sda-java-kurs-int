package companyManager.fileoperation.writer;

import companyManager.Company;
import companyManager.Employee;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;

public class XmlEmployeeWriter extends AbstractEmployeeWriter {
    public XmlEmployeeWriter(String pathToFile) {
        super(pathToFile);
    }

    @Override
    public void writeEmployees(Employee[] employees) {
//        dzieki temu obejsciu bedziemy mogli pobrac pracownikow z tablicy
        Company companyToSave = new Company();
        companyToSave.setEmployees(employees);
        try {
//            JAXBContext to klasa opakowywujaca nasze odnośniki xml opisujace pola w innych klasach - mechanizm refleksji
            JAXBContext context = JAXBContext.newInstance(Company.class);
//            obiekt zapisujacy do xml - marshaller, w drugą stronę to jest unmarshaller
            Marshaller marshaller = context.createMarshaller();
//            poprawia czytelność pliku
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(companyToSave, new File(pathToFile));
        } catch (JAXBException e){
                e.printStackTrace();
            }
    }
}
