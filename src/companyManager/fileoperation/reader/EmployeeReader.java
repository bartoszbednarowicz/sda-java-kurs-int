package companyManager.fileoperation.reader;
import companyManager.Employee;

public interface EmployeeReader {
    Employee[] readEmployees();
}


