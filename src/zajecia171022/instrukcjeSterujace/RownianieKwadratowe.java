package zajecia171022.instrukcjeSterujace;

import java.util.Scanner;

public class RownianieKwadratowe {
    public static void main(String[] args) {

        int a;
        int b;
        int c;
        System.out.println("Podaj wartości współczynników a");
        Scanner odczyt = new Scanner(System.in);
        a = odczyt.nextInt();
        System.out.println("Podaj wartości współczynników b");
        b = odczyt.nextInt();
        System.out.println("Podaj wartości współczynników c");
        c = odczyt.nextInt();

        int delta = b * b - 4 * a * c;

        if (delta<0) {
            System.out.println("Brak rozwiązań w zbiorze liczb rzeczywistych");
        } else if (delta == 0) {
            double wynik = (double)-b / (2 * a);
            System.out.println("Równanie posiada jedno rozwiązanie: " + wynik);
        } else {
            double wynik1 = ((double)-b - Math.sqrt(delta)) / 2*a;
            double wynik2 = ((double)-b + Math.sqrt(delta)) / 2*a;
            double pierwistek = Math.sqrt (delta);
            System.out.println("Równianie posiada dwa rozwiązania: " + wynik1 + " " + wynik2);
        }



    }
}
