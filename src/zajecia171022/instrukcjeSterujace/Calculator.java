package zajecia171022.instrukcjeSterujace;

import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {

        double a;
        double b;
        double wynik;

        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj wartość a");
        a = odczyt.nextDouble();
        System.out.println("Podaj wartość b");
        b = odczyt.nextDouble();

        System.out.println("---------------");
        System.out.println("Wybierz opcję:");
        System.out.println("1. Dodawanie");
        System.out.println("2. Odejmowanie");
        System.out.println("3. Mnożenie");
        System.out.println("4. Dzielenie");

        int wybor = odczyt.nextInt();


        switch (wybor) {
            case 1:
                wynik = a + b;
                System.out.println(wynik);
                break;
            case 2:
                wynik = a - b;
                System.out.println(wynik);
                break;
            case 3:
                wynik = a * b;
                System.out.println(wynik);
                break;
            case 4:
                wynik = a / b;
                System.out.println(wynik);
                break;
            default:
                System.out.println("Wybierz wartość od 1 do 4");
                break;
        }
    }
}
