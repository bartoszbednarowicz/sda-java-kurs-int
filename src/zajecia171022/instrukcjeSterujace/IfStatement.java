package zajecia171022.instrukcjeSterujace;

import java.util.Scanner;

public class IfStatement {
    public static void main(String[] args) {

        int wiek;

        System.out.println("Jaki masz wiek?");
        Scanner odczyt = new Scanner(System.in);
        wiek = odczyt.nextInt();

        if (wiek >= 18) {
            System.out.println("Jesteś pełnoletni");
        } else {
            System.out.println("Jesteś niepełnoletni- ograniczony dostęp");
        }

        System.out.println("Witamy na stronie!");

    }
}
