package zajecia171022.instrukcjeSterujace;

import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {

        double waga;
        double wzrost;
        double bmi;

        System.out.println("Podaj swoją masę w kg");
        Scanner odczyt = new Scanner(System.in);
        waga = odczyt.nextDouble();

        System.out.println("Podaj swój wzrost w metrach");
        wzrost = odczyt.nextDouble();
/*
    Potęga i obcięcie do dwóch miejsc po przecinku.
*/
        bmi = (waga / Math.pow(wzrost, 2));
        System.out.println("BMI: " + String.format("%.2f",bmi));

        if (bmi <= 18.5) {
            System.out.println("Niedowaga");
        } else if ((bmi > 18.5) && (bmi <= 24.9)) {
            System.out.println("Waga poprawna");
        } else if ((bmi > 24.9) && (bmi <= 29)) {
            System.out.println("Nadwaga");
        } else {
            System.out.println("Otyłość");
        }
    }

}


