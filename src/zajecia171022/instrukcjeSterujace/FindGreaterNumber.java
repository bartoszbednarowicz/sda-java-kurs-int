package zajecia171022.instrukcjeSterujace;

import java.util.Scanner;

public class FindGreaterNumber {
    public static void main(String[] args) {

        int a;
        int b;

        System.out.println("Podaj liczbę A");
        Scanner odczyt = new Scanner(System.in);
        a = odczyt.nextInt();

        System.out.println("Podaj liczbę B");
        b = odczyt.nextInt();

        if ( a > b ) {
            System.out.println("Liczba A jest większa: " + a);
        } else if ( a == b ) {
            System.out.println("Liczby są równe.");
        } else {
            System.out.println("Liczba B jest większa: " + b);
        }

    }
}
