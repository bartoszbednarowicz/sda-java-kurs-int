package zajecia171022.OdczytZKonsoli;

import java.util.Scanner;

//  Program pobierający dwie liczby i obliczający wyrażenie systemu (a>b)&&(a!=b)

public class OperatoryLogiczneIntro {
    public static void main(String[] args) {

        int a;
        int b;

        Scanner odczyt = new Scanner(System.in);
        System.out.println("Podaj liczbę a");
        a = odczyt.nextInt();

        System.out.println("Podaj wartość wyrażenia b");
        b = odczyt.nextInt();

        boolean wynik = (a >= b) && (a != b);

        System.out.println("Wyrażenie jest: " + wynik);
    }
}
