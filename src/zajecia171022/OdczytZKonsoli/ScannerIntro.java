package zajecia171022.OdczytZKonsoli;

import java.util.Scanner;

/*
Program pobierający dane i drukujący je w konsoli
Zapytaj o imię
*/

public class ScannerIntro {
    public static void main (String[] args) {

        String imie;
        String nazwisko;
        int wiek;

        System.out.println("Jak masz na imię?");

//  Utworzenie obiektu typu scanner o nazwie odczyt
        Scanner odczyt = new Scanner(System.in);

//  Do zmiennej imie wstaw nastepną linię z konsoli
        imie = odczyt.nextLine();

        System.out.println("Witaj, " + imie);

        System.out.println("Podaj nazwisko");
        nazwisko = odczyt.nextLine();

        System.out.println("Podaj wiek");
        wiek = odczyt.nextInt();

        System.out.println("Witaj " + imie + " " + nazwisko + ". Masz " + wiek + " lata.");




    }

}
