package zajecia171022.OdczytZKonsoli;

import java.util.Scanner;

public class Konwerter {
    public static void main(String[] args) {

        float cel;
        System.out.println("Podaj wartość temperatury w stopniach Celsjusza");
        Scanner odczyt = new Scanner(System.in);
        cel = odczyt.nextFloat();

        System.out.println("Wartość w F: " + (cel * 1.8 + 32));

//      Ucinanie wyniku do tylko dwóch miejsc po przecinku

        System.out.println("Wartość w F: " + String.format("%.2f",(cel * 1.8 + 32)));

    }
}
