package zajecia171118.zadanie6;

public class Application {
    public static void main(String[] args) {

        Person person = new Person("Bartek", "Bednarowicz", 32);
        System.out.println(person.describe());

        Student student = new Student("Jan", "Kowalski", 54);
        System.out.println(student.describe());
        System.out.println(student.getUniversity());

        Student studentInformatyki = new Student("Tomek", "As", 32, "45KJ", "Politechnika");
        System.out.println(studentInformatyki.describe());
        System.out.println(studentInformatyki);

        Worker worker = new Worker("Marek", "Kowalski", 25, "20100", "Dyrektor");
        System.out.println(worker.toString());
    }
}
