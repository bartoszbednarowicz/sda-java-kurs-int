package zajecia171118.zadanie6;

public class Worker extends Person{

    private String salary;
    private String position;

    public Worker(String name, String surname, int age) {
        super(name, surname, age);
    }

    public Worker(String name, String surname, int age, String salary, String position) {
        super(name, surname, age);
        this.salary = salary;
        this.position = position;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public String describe () {
        String opisPracownik = super.describe();
        return String.format("%s %s jest pracownikiem i zarabia: " + getSalary() + " PLN");
    }

    @Override
    public String toString() {
        return super.toString() + salary + "/" + position;
    }
}
