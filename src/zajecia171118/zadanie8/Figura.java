package zajecia171118.zadanie8;

public abstract class Figura {

    public abstract double obliczPole();

    public abstract double obliczObwod();

}
