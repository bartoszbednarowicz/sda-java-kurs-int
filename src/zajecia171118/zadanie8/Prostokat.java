package zajecia171118.zadanie8;

public class Prostokat extends Figura {
    private double a;
    private double b;

    public Prostokat (double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    @Override
    public double obliczPole() {
        System.out.println("Pole");
        return a * b;
    }

    @Override
    public double obliczObwod() {
        System.out.println("Obwód");
        return 2 * a + 2 * b;
    }
}
