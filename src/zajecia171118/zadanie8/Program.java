package zajecia171118.zadanie8;

import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int ilosc = 0;
        Figura[] tablicaFigur = new Figura[10];
        boolean czyWyjsc = false;
        while (!czyWyjsc) {
            System.out.println("Wybierz nowa fagure:");
            System.out.println("1. Kwadrat");
            System.out.println("2. Prostokat");
            System.out.println("3. Kolo");
            System.out.println("4. Pola Wszystkich");
            System.out.println("5. Wyjscie");
            int wybor = scanner.nextInt();
            Figura figura;
            switch (wybor) {
                case 1: {
                    System.out.println("Podaj bok kwadratu");
                    int bok = scanner.nextInt();
                    figura = new Kwadrat(bok);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 2: {
                    System.out.println("Podaj boki prostokata");
                    double bokA = scanner.nextInt();
                    double bokB = scanner.nextInt();
                    figura = new Prostokat(bokA, bokB);
                    tablicaFigur[ilosc++] = figura;
                    break;
                }
                case 3: {
                    System.out.println("Funkcja w przygotowaniu");
                    break;
                }
                case 4: {
                    System.out.println("Pola wszystkich");
                    for (int i = 0; i < ilosc; i++) {
                        Figura f = tablicaFigur[i];
                        System.out.println(f.obliczPole());
                        System.out.println();
                    }
                    break;
                }
                case 5: {
                    czyWyjsc = true;
                    break;
                }
            }

        }
    }
}
