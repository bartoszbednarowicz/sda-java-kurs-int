package zajecia171118.zadanie8;

public class Kwadrat extends Figura {
    private double a;

    public Kwadrat(double a) {
        this.a = a;
    }

    @Override
    public double obliczPole() {
        System.out.println("Pole");
        return a * a;
    }

    @Override
    public double obliczObwod() {
        System.out.println("Obwód");
        return 4*a;
    }

}