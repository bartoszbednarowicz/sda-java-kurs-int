package zajecia171118.zadanie5;

import java.util.Random;

public class Matrix {

//    klasa obejmuje jedna maciez, ktora opsuja pola ponizej
    private int[][] matrix;
    private int x;
    private int y;


//    tworzymy konstruktor przeciazony, bezparametrowy bedzie dodany podczas kompilacji

    public Matrix(int x, int y) {
        this.x = x;
        this.y = y;
        matrix = new int[x][y];
    }

//    do tablicy nie robimy gettera i settera - dzialania beda wykonywane przy pomocy metod
//    this wskazuje, ze zmienne beda pobierane z tej klasy


    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

//    metoda do dodawana maciezy

    public Matrix addMatrix(Matrix second) {

//        najpierw sprawdzic czy wymianry drugiej poprawne
        if (this.x != second.x || this.y != second.y) {
            System.out.println("Wyjątek: nie można dodawać");
            throw new ArithmeticException("Wymiary macierzy się nie zgadzają");
        }
//        przejsc do obliczen
        Matrix result = new Matrix(this.x, this.y);
        for (int i = 0; i < x; i++){
            for (int j = 0; j < y; j++){
                result.matrix[i][j] = this.matrix[i][j] + second.matrix[i][j];
            }
        }
        return result;
    }
    /*
    * metoda do drukowania matrix
    *
    * */
    public void print() {
        for (int i = 0; i < x; i++){
            for (int j = 0; j < y; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }

    }

    public void filWithRandomValues() {
        Random random = new Random();
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                matrix[i][j] = random.nextInt(50);
            }
        }
    }
}
