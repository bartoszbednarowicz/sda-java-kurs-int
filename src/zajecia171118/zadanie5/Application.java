package zajecia171118.zadanie5;

public class Application {
    public static void main(String[] args) {
        Matrix first = new Matrix(3, 3);
        first.filWithRandomValues();
        first.print();
        System.out.println();

        Matrix second = new Matrix(3, 3);
        second.filWithRandomValues();
        second.print();
        System.out.println();
        System.out.println("Result: ");

        try {
            Matrix result = first.addMatrix(second);
            result.print();
            System.out.println(result.toString());
        } catch (ArithmeticException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
